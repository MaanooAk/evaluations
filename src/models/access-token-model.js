import {DataObject, EdmMapping} from '@themost/data';

@EdmMapping.entityType('AccessToken')
class AccessToken extends DataObject {

    constructor() {
        super();
    }

    static async inspect(context, access_token) {
        const token = await context.model('AccessToken')
          .where('access_token').equal(access_token)
          .silent()
          .getTypedItem();
        if (token == null) {
          return {
            active: false
          }
        }
        return {
          active: !token.isExpired(),
          username: token.user_id,
          client_id: token.client_id,
          access_token: token.access_token,
          refresh_token: token.refresh_token,
          scope: token.scope
        };
      }

      isExpired() {
          if (this.expires == null) {
              return true;
          }
          // check only date parts
          const currentDate = new Date().setHours(0, 0, 0, 0);
          const expires = this.expires.setHours(0, 0, 0, 0);
          return (expires < currentDate);
      }
}

module.exports = AccessToken;
