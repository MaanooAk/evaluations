import {EdmMapping, EdmType} from '@themost/data';
import EvaluationEvent from "./evaluation-event-model";
import {DataError, TraceUtils, DataNotFoundError} from "@themost/common";
import {getMailer} from "@themost/mailer";
import moment from "moment";

/**
 * @class
 */
@EdmMapping.entityType('ClassInstructorEvaluationEvent')
class ClassInstructorEvaluationEvent  extends EvaluationEvent {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    @EdmMapping.func('Results', EdmType.CollectionOf('Object'))
    async getResults() {
        // check access to ClassInstructorEvaluationEvent and get results silently
        const event = await this.context.model('ClassInstructorEvaluationEvent').where('id').equal(this.getId());
        if (event) {
            const form = await this.getForm();
            return this.context.model(form.properties.name).where('evaluationEvent').equal(this.id).silent().prepare();
        }
    }
    @EdmMapping.action('GenerateEvaluationTokens', 'GenerateInstructorTokenAction')
     async generateEvaluationTokens() {
        const context = this.context;
        const action = {
            event: this.id,
            numberOfStudents: 0,
            total: 0,
            actionStatus: {alternateName: 'PotentialActionStatus'}
        };
        // get courseClass instructor
        const event = await context.model('ClassInstructorEvaluationEvent').where('id').equal(this.id)
            .expand({
                name: 'courseClassInstructor',
                options: {
                    '$expand': 'courseClass'
                }
            })
            .getItem();
        if (event) {
            //check if a GenerateInstructorTokenAction exists
            const prevAction = await context.model('GenerateInstructorTokenAction').where('event').equal(this.id)
                .and('actionStatus/alternateName').notEqual('FailedActionStatus')
                .silent().getItem();
            if (prevAction) {
                throw new Error('A previous action for generating evaluation tokens has been found.');
            }
            const courseClass = event.courseClassInstructor && event.courseClassInstructor.courseClass;
            const eventInstructor = event.courseClassInstructor && event.courseClassInstructor.instructor;
            // get class sections
            const sections = await context.model('CourseClassSection').where('courseClass').equal(courseClass.id).getItems();
            // check if course class has sections
            if (sections && sections.length > 0 && courseClass.mustRegisterSection !== 0) {
                // get only number of students registered at specific section
                const instructorSections = await context.model('CourseClassSectionInstructor').where('courseClass').equal(courseClass.id)
                    .and('instructor').equal(eventInstructor).select('section/section as sectionId').silent().getItems();
                if (instructorSections && instructorSections.length === 0) {
                    action.actionStatus.alternateName = 'FailedActionStatus';
                    action.description = 'Instructor is not related with any of sections';
                    return await context.model('GenerateInstructorTokenAction').save(action);
                } else {
                    const sectionIds = instructorSections.map(section => {
                        return section.sectionId;
                    });
                    // get number of students for specific course class sections
                    action.numberOfStudents = await context.model('StudentCourseClass').where('courseClass').equal(courseClass.id)
                        .and('section').in(sectionIds)
                        .silent().count();
                }
            } else {
                action.numberOfStudents = await context.model('StudentCourseClass').where('courseClass').equal(courseClass.id).silent().count();
            }
            // save action and then generateTokens
            const generateAction = await context.model('GenerateInstructorTokenAction').save(action);
            const generateOptions =
                {
                    evaluationEvent: event.id,
                    n: action.numberOfStudents,
                    expires : event.endDate
                };
            try {
                // save number of students to event
                event.numberOfStudents = action.numberOfStudents;
                await context.model('ClassInstructorEvaluationEvent').save(event);
                if (event.numberOfStudents>0) {
                    await EvaluationEvent.generateTokens(context, generateOptions);
                }
                generateAction.total = action.numberOfStudents;
                generateAction.actionStatus = {alternateName: 'CompletedActionStatus'}
                return await context.model('GenerateInstructorTokenAction').save(generateAction);
            }
            catch (err) {
                TraceUtils.error(err);
                // update also action status
                generateAction.total = 0;
                generateAction.actionStatus = {alternateName: 'FailedActionStatus'}
                generateAction.description = err.message;
                return await context.model('GenerateInstructorTokenAction').save(generateAction);

            }
        }
    }
    @EdmMapping.func('TokenInfo', EdmType.CollectionOf('Object'))
    async getTokenStatistics() {
        const tokenInfo = {
            total: 0,
            used: 0,
            sent: 0
        };
        // get number if tokens
        const tokens = await this.context.model('EvaluationAccessToken').where('evaluationEvent').equal(this.getId())
            .select('count(evaluationEvent) as total', 'used', 'sent')
            .groupBy('used', 'sent', 'evaluationEvent')
            .silent().getItems();
        tokens.map(item => {
            tokenInfo.total += item.total;
            tokenInfo.used += item.used ? item.total : 0;
            tokenInfo.sent += item.sent ? item.total : 0;
        });
        return tokenInfo;
    }

    @EdmMapping.action('SendEvaluationTokens', 'SendInstructorTokenAction')
    async sendEvaluationTokens() {
        const context = this.context;
        // get event and check if event endDate has passed
        // get current date
        const currentDate = moment(new Date()).startOf('day').toDate(); // get only date format
        const event = await context.model('ClassInstructorEvaluationEvent').where('id').equal(this.getId())
            .and('date(endDate)').greaterOrEqual(currentDate)
            .expand({
                name: 'courseClassInstructor',
                options: {
                    '$expand': 'courseClass($expand=course), instructor' // expand course and instructor so their attributes are available in the mail template
                }
            },
            {
                name: 'actions',
                options: {
                    '$expand': 'actionStatus'
                }
            })
            .getItem();
        // validate event
        if (event == null) {
            throw new DataNotFoundError('The event cannot be found or is inaccessible.');
        }
        // validate status
        if (!(event.eventStatus && event.eventStatus.alternateName === 'EventOpened')) {
            throw new DataError('Send tokens action may only be executed for Open/Active events.');
        }
        // validate event actions
        if (!(event.actions && event.actions.length)) {
            throw new DataNotFoundError('There are not generate token actions for the specified event or they are inaccessible.');
        }
        // find completed token generation action (can only be one)
        const successfulTokenAction = event.actions.find(generateTokenAction => generateTokenAction.actionStatus.alternateName === 'CompletedActionStatus');
        if (successfulTokenAction == null) {
            throw new DataError('The specified event does not have a completed token generation action.');
        }
        // get tokenInfo for this event
        const tokenInfo = await this.getTokenStatistics();
        // validate sent-used seperately for descriptive errors
        if (tokenInfo.sent) {
            throw new DataError(`Cannot proceed with sending tokens, because ${tokenInfo.sent} of them is/are already sent.`);
        }
        if (tokenInfo.used) {
            throw new DataError(`Cannot proceed with sending tokens, because ${tokenInfo.used} of them is/are already used.`);
        }
        // start gathering students
        let students;
        const courseClass = event.courseClassInstructor && event.courseClassInstructor.courseClass;
        const eventInstructor = event.courseClassInstructor && event.courseClassInstructor.instructor;
        // get class sections
        const sections = await context.model('CourseClassSection').where('courseClass').equal(courseClass.id).getItems();
        // check if course class has sections
        if (sections && sections.length > 0 && courseClass.mustRegisterSection !== 0) {
            // get only number of students registered at specific section
            const instructorSections = await context.model('CourseClassSectionInstructor').where('courseClass').equal(courseClass.id)
                .and('instructor').equal(eventInstructor).select('section/section as sectionId').silent().getItems();
            if (instructorSections && instructorSections.length === 0) {
                throw new DataError('Instructor is not related with any of sections');
            } 
            const sectionIds = instructorSections.map(section => {
                return section.sectionId;
            });
            // get students for specific course class sections
            students = await context.model('StudentCourseClass')
                .where('courseClass').equal(courseClass.id)
                .and('section').in(sectionIds)
                .select('email')
                .silent()
                .getAllItems();
        } else {
            students = await context.model('StudentCourseClass')
                .where('courseClass').equal(courseClass.id)
                .select('email')
                .silent()
                .getAllItems();
        }
        if (!(Array.isArray(students) && students.length)) {
            throw new DataError('No recipients/students found.');
        }
        // check total of students
        if (students.length !== tokenInfo.total) {
            throw new DataError(`Cannot proceed to sending tokens. Students are ${students.length} and generated tokens are ${tokenInfo.total}.`);
        }
        // validate student emails, must all exist and have a valid format
        // https://html.spec.whatwg.org/multipage/input.html#valid-e-mail-address
        const mailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
        const invalidEmail = students.find(student => {
            return !(student.email && mailRegex.test(student.email));
        });
        if (invalidEmail) {
            throw new DataError(`Cannot procced to sending tokens because at least one student's email is null or invalid. Found invalid email: ${invalidEmail.email || 'empty'}`);
        }
        // get tokens
        let tokens = await this.context.model('EvaluationAccessToken').where('evaluationEvent').equal(this.getId())
            .select('access_token')
            .silent()
            .getAllItems();
        // and map them to an array
        tokens = tokens.map(token => token.access_token);
        // get mail template
        const mailTemplate = await context.model('MailConfiguration')
            .where('target').equal('SendInstructorTokenAction')
            .silent()
            .getItem();
        if (mailTemplate == null) {
            throw new DataError('Cannot execute send tokens action because template is missing for SendInstructorTokenAction.');
        }
        const succeeded = [];
        const failed = [];
        try {
            // format mail subject expected params ${course}, ${instructor}
            const exp = new RegExp('\\$\\{\\w+\\}+','ig');
            let subject = mailTemplate.subject;
            const params = subject.match(exp);
            if (params.length>0) {
                // parse only custom parameters ${instructor},${course}
                params.forEach(param => {
                    if (param === '${instructor}') {
                        // get instructor full name
                        subject = subject.replace(param, eventInstructor? `${eventInstructor.familyName} ${eventInstructor.givenName}` : '');
                    } else if (param === '${course}') {
                        // get courseClass code and title
                        subject = subject.replace(param, courseClass.course ? `${courseClass.course.displayCode} ${courseClass.title}` : '');
                    } else {
                        subject = subject.replace(param, '');
                    }
                });
            }
            // use an async function to send emails
            await (async () => {
                for (const student of students) {
                    // refresh mailer
                    const mailer = getMailer(context); // maybe investigate why this has to be refreshed.
                    // pop an access token
                    const access_token = tokens.pop();
                    // send message
                    await new Promise(resolve => {
                        mailer
                            .template(mailTemplate.template)
                            .subject(subject)
                            .to(student.email)
                            .send(Object.assign(event, {
                                access_token
                            }), (err) => {
                                if (err) {
                                    try {
                                        TraceUtils.error(`SendEvaluationTokensAction', 'An error occurred while trying to send token to student email ${student.email}`);
                                        TraceUtils.error(err);
                                    } catch (err1) {
                                        // do nothing
                                    }
                                    failed.push(access_token);
                                    return resolve();
                                }
                                succeeded.push(access_token);
                                return resolve();
                            });
                    });
                }
            })();
            // update successfully sent tokens
            for (const succededToken of succeeded) {
                await context.model('EvaluationAccessToken').silent().save({
                    access_token: succededToken, // access_token is primary
                    sent: true,
                    $state: 2
                });
            }
            // create send tokens action
            const sendTokensAction = {
                event: this.getId(),
                numberOfStudents: students.length,
                total: tokenInfo.total,
                sent: succeeded.length,
                failed: failed.length,
                actionStatus: {
                    alternateName: 'CompletedActionStatus'
                }
            }
            // and save
            return await context.model('SendInstructorTokenAction').silent().save(sendTokensAction);
        } catch (err) {
            // log error
            TraceUtils.error(err);
            // update successfully sent tokens
            for (const succededToken of succeeded) {
                await context.model('EvaluationAccessToken').silent().save({
                    access_token: succededToken, // access_token is primary
                    sent: true,
                    $state: 2
                });
            }
            // create send tokens action
            const sendTokensAction = {
                event: this.getId(),
                numberOfStudents: students.length,
                total: tokenInfo.total,
                sent: succeeded.length,
                failed: succeeded.length ? tokenInfo.total - succeeded.length : tokenInfo.total,
                actionStatus: {
                    alternateName: 'CompletedActionStatus'
                }
            }
            // and save
            return await context.model('SendInstructorTokenAction').silent().save(sendTokensAction);
        }
    }
}
module.exports = ClassInstructorEvaluationEvent;
